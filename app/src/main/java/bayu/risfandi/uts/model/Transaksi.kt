package bayu.risfandi.uts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Transaksi (var idTransaksi: String, var id_user : String,var total : String,
                 var bukti_pembayaran : String, var status : String, var tgl : String) :
    Parcelable {
    constructor() : this("","","","","","")
}