package bayu.risfandi.uts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class DetTransaksi(var idDetTransaksi: String, var id_transaksi : String,var id_product : String,var id_user : String, var qty : String) :
    Parcelable {
    constructor() : this("", "","","","")
}