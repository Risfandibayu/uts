package bayu.risfandi.uts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Produk (var idProduk : String, var merkProduk : String, var hargaProduk : String,
              var deskProduk : String, var stokProduk : Int, var namaFile : String,var fileProduk : String) :
    Parcelable {
    constructor() : this("","","","",0,"","")

}