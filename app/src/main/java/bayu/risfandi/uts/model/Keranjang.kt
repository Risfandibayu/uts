package bayu.risfandi.uts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Keranjang (var idKeranjang: String, var idProduk : String, var namaProduk : String, var hargaProduk : String, var idUser : String, var qty : Int) :
    Parcelable {
    constructor() : this("","","","","",0)
}