package bayu.risfandi.uts.screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import bayu.risfandi.uts.MainActivity
import bayu.risfandi.uts.R
import bayu.risfandi.uts.util.Komponen
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initView()
    }

    private fun initView() {
        textView_button_to_register.setOnClickListener {
            val intent = Intent(this,RegisterActivity::class.java)
            startActivity(intent)
        }
        button_login_login.setOnClickListener {
            loginUser()
        }
    }

    private fun loginUser(){
        Komponen.ProgressLoading(true,"Processing",this)
        val email = editText_Email_Login.text.toString()
        val password = editText_Password_Login.text.toString()
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if(it.isSuccessful) {
                    Komponen.ProgressLoading(false,"",this)
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }else{
                    return@addOnCompleteListener
                }
            }
            .addOnFailureListener {
                Komponen.ProgressLoading(false,"",this)
                Toast.makeText(this,"Failed to login : ${it.message}", Toast.LENGTH_SHORT).show()
            }
    }
}