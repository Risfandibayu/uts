package bayu.risfandi.uts.screen.produk

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Produk
import bayu.risfandi.uts.util.Komponen
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_edit_produk.*
import java.text.SimpleDateFormat
import java.util.*

class EditProdukActivity : AppCompatActivity() {
    lateinit var uri : Uri
    var isInputFile : Boolean = false

    companion object {
        val K_IDPRODUK = "idProduk"
        val K_MERKPRODUK = "merkProduk"
        val K_HARGAPRODUK = "hargaProduk"
        val K_DESKPRODUK = "deskProduk"
        val K_STOKPRODUK = "stokProduk"
        val K_FILEPRODUK = "fileProduk"
        val K_NAMAFILE =  "namaFile"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_produk)

        initView()
    }

    private fun initView() {
        supportActionBar?.title = "Edit Produk"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val produk = intent.getParcelableExtra<Produk>("DetProduk")
        editText_Merk_HP_EditProduk.setText(produk?.merkProduk)
        editText_Stok_Barang_Edit_Produk.setText(produk?.stokProduk.toString())
        editText_Harga_Produk_EditProduk.setText(produk?.hargaProduk)
        editText_selected_foto_produk_edit_produk.setText(produk?.fileProduk)
        editText_deskripsi_produk_edit_produk.setText(produk?.deskProduk)

        button_simpan_produk_edit_produk.setOnClickListener {
            uploadDataProduk()
        }

        button_tambah_foto_edit_produk.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_GET_CONTENT
            intent.setType("image/*")
            startActivityForResult(intent, 100)
        }
    }

    private fun uploadDataProduk() {
        val produkId = intent.getParcelableExtra<Produk>("DetProduk")

        Komponen.ProgressLoading(true,"Harap Tunggu...",this)
        val fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
        val fileRef = FirebaseStorage.getInstance().reference.child(fileName+".jpg")
        if(isInputFile){
            // Delete File Sebelumnya
            FirebaseStorage.getInstance().reference.child(produkId?.namaFile.toString()).delete()
                .addOnSuccessListener {
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { task ->
                            val hm = HashMap<String, Any>()
                            hm.set(K_IDPRODUK, produkId?.idProduk.toString())
                            hm.set(K_MERKPRODUK, editText_Merk_HP_EditProduk.text.toString())
                            hm.set(K_HARGAPRODUK, editText_Harga_Produk_EditProduk.text.toString())
                            hm.set(K_DESKPRODUK, editText_deskripsi_produk_edit_produk.text.toString())
                            hm.set(K_STOKPRODUK, editText_Stok_Barang_Edit_Produk.text.toString())
                            hm.set(K_NAMAFILE,fileName+".jpg")
                            hm.set(K_FILEPRODUK, task.result.toString())
                            updateDataProduk(produkId?.idProduk.toString(),hm)
                        }
                }
                .addOnFailureListener {
                    Toast.makeText(this,it.message.toString(), Toast.LENGTH_LONG).show()
                }
        }else{
            val hm = HashMap<String, Any>()
            hm.set(K_IDPRODUK, produkId?.idProduk.toString())
            hm.set(K_MERKPRODUK, editText_Merk_HP_EditProduk.text.toString())
            hm.set(K_HARGAPRODUK, editText_Harga_Produk_EditProduk.text.toString())
            hm.set(K_DESKPRODUK, editText_deskripsi_produk_edit_produk.text.toString())
            hm.set(K_STOKPRODUK, editText_Stok_Barang_Edit_Produk.text.toString())
            updateDataProduk(produkId?.idProduk.toString(),hm)
        }
    }

    private fun updateDataProduk(produkId : String, hm : HashMap<String, Any>){
        FirebaseFirestore.getInstance().collection("products").document(produkId).update(hm)
            .addOnSuccessListener {
                Komponen.ProgressLoading(false,"",this)
                onBackPressed()
                Toast.makeText(this,"Berhasil mengubah produk",Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener {
                Toast.makeText(this, "Data failed to added ${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if((resultCode == Activity.RESULT_OK) && (requestCode == 100)){
            if(data != null){
                isInputFile = true
                Toast.makeText(this,isInputFile.toString(), Toast.LENGTH_LONG).show()
                uri = data.data!!
                editText_selected_foto_produk_edit_produk.setText(uri.toString())
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}