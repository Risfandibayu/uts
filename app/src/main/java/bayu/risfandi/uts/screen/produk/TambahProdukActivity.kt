package bayu.risfandi.uts.screen.produk

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Produk
import bayu.risfandi.uts.util.Komponen
import bayu.risfandi.uts.util.RandomAngka
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_tambah_produk.*
import java.text.SimpleDateFormat
import java.util.*

class TambahProdukActivity : AppCompatActivity() {

    lateinit var uri : Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_produk)

        initView()
    }

    private fun initView() {
        supportActionBar?.title = "Tambah Produk"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        button_tambah_foto_tambah_produk.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_GET_CONTENT
            intent.setType("image/*")
            startActivityForResult(intent, 100)
        }

        button_simpan_produk.setOnClickListener {
            uploadDataProduk()
        }
    }

    private fun uploadDataProduk() {
        val merkProduk = editText_Merk_HP_TambahProduk.text.toString()
        val hargaProduk = editText_Harga_Produk_TambahProduk.text.toString()
        val deskripsiProduk = editText_deskripsi_produk_tambah_produk.text.toString()
        val stokProduk = editText_Stok_Barang_Tambah_Produk.text.toString()

        Komponen.ProgressLoading(true,"Harap Tunggu...",this)
        val fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
        val fileRef = FirebaseStorage.getInstance().reference.child(fileName+".jpg")
        fileRef.putFile(uri)
            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                return@Continuation fileRef.downloadUrl
            })
            .addOnCompleteListener { task ->
                val idProduk = RandomAngka.getRandomString(15)
                FirebaseFirestore.getInstance().collection("products").document(idProduk).set(
                    Produk(idProduk,merkProduk,hargaProduk,deskripsiProduk,stokProduk.toInt(),fileName+".jpg",task.result.toString())
                )
                    .addOnSuccessListener {
                        Komponen.ProgressLoading(false,"",this)
                        onBackPressed()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "Data failed to added ${it.message}", Toast.LENGTH_LONG).show()
                    }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if((resultCode == Activity.RESULT_OK) && (requestCode == 100)){
            if(data != null){
                uri = data.data!!
                editText_selected_foto_produk_tambah_produk.setText(uri.toString())
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}