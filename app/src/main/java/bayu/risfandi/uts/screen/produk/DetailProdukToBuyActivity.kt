package bayu.risfandi.uts.screen.produk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Keranjang
import bayu.risfandi.uts.model.Produk
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_produk_to_buy.*
import java.text.SimpleDateFormat
import java.util.*

class DetailProdukToBuyActivity : AppCompatActivity() {

    val keranjang = Keranjang()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_produk_to_buy)

        initView()
    }

    private fun initView() {
        val produk = intent.getParcelableExtra<Produk>("DetProduktoBuy")

        supportActionBar?.title = produk!!.merkProduk
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        Picasso.get().load(produk?.fileProduk).into(imageView_Det_ToSell)
        textView_Produk_toSell.setText(produk!!.merkProduk)
        textView_Harga_toSell.setText(produk!!.hargaProduk)
        textView_Stok_toSell.setText("Stok tersisa "+produk!!.stokProduk)
        textView_Deskripsi_toDrll.setText(produk!!.deskProduk)

        button_tambah_keranjang_toSell.setOnClickListener {
            addToCart()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun addToCart(){
        val produk = intent.getParcelableExtra<Produk>("DetProduktoBuy")
        val uid = FirebaseAuth.getInstance().uid
        val idkeranjang = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())+produk!!.idProduk

        keranjang.idKeranjang = idkeranjang
        keranjang.idProduk = produk!!.idProduk
        keranjang.hargaProduk = produk!!.hargaProduk
        keranjang.idUser = uid.toString()
        keranjang.qty = 1

        FirebaseFirestore.getInstance().collection("keranjang").document(idkeranjang).set(keranjang).addOnSuccessListener {
            Toast.makeText(this, "Behasil menambahkan ke keranjang", Toast.LENGTH_LONG).show()
        }.addOnFailureListener {
            Toast.makeText(this, it.message.toString(), Toast.LENGTH_LONG).show()
        }
    }
}