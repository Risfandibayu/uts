package bayu.risfandi.uts.screen.produk

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Produk
import bayu.risfandi.uts.util.Komponen
import bayu.risfandi.uts.util.RowProduk
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_produk.*

class DetailProdukActivity : AppCompatActivity() {
    val produkCrud = Produk()
    val rowProduk = RowProduk()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_produk)

        initView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_detail, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.menu_edit -> {
                val produk = intent.getParcelableExtra<Produk>("DetProduk")
                val intent = Intent(this, EditProdukActivity::class.java)
                intent.putExtra("DetProduk", produk)
                startActivity(intent)
            }
            R.id.menu_hapus -> {
                val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                builder.setMessage("Apakah anda yakin mau menghapus?")
                    .setCancelable(false)
                    .setPositiveButton("Ya", DialogInterface.OnClickListener { dialog, id -> deleteData() })
                    .setNegativeButton("Tidak", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() }) //Set your icon here
                    .setTitle("Hapus Data!")
                    .setIcon(R.drawable.ic_warning)
                val alert: AlertDialog = builder.create()
                alert.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        supportActionBar?.title = "Detail Produk"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val produk = intent.getParcelableExtra<Produk>("DetProduk")

        Picasso.get().load(produk?.fileProduk).into(imageView_Produk_Detail)
        produkCrud.idProduk = produk?.idProduk.toString()
        editTextMerek_Produk_Detail.setText(produk?.merkProduk)
        editText_Stok_Produk.setText(produk?.stokProduk.toString())
        editText_Harga.setText(produk?.hargaProduk)
        editText_Deskripsi_Detail_Produk.setText(produk?.deskProduk)
    }

    private fun deleteData(){
        val produk = intent.getParcelableExtra<Produk>("DetProduk")
        FirebaseStorage.getInstance().reference.child(produk?.namaFile.toString()).delete()
            .addOnSuccessListener {
                FirebaseFirestore.getInstance().collection("products").whereEqualTo(rowProduk.K_IDPRODUK,produk?.idProduk).get()
                    .addOnSuccessListener { results ->
                        for (doc in results){
                            FirebaseFirestore.getInstance().collection("products").document(doc.id).delete()
                                .addOnSuccessListener {
                                    Komponen.ProgressLoading(false,"",this)
                                    onBackPressed()
                                }
                                .addOnFailureListener {
                                    Toast.makeText(this, "Data failed deleted", Toast.LENGTH_LONG).show()
                                }
                        }
                    }.addOnFailureListener { e ->
                        Toast.makeText(this, "Can't get data references ${e.message}", Toast.LENGTH_LONG).show()
                    }
            }
            .addOnFailureListener {
                Toast.makeText(this,it.message.toString(), Toast.LENGTH_LONG).show()
            }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}