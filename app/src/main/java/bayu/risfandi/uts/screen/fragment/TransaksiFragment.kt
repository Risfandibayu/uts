package bayu.risfandi.uts.screen.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bayu.risfandi.uts.MainActivity
import bayu.risfandi.uts.R
import bayu.risfandi.uts.adapter.TransaksiAdapter
import bayu.risfandi.uts.model.Transaksi
import bayu.risfandi.uts.screen.transaksi.DetailTransaksiActivity
import bayu.risfandi.uts.util.RowTransaksi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_transaksi.view.*


class TransaksiFragment : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View
    var listTransaksi : MutableList<Transaksi> = ArrayList()
    val rowTransaksi = RowTransaksi()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_transaksi, container, false)
        return v
    }

    private fun showListTransaksi(){
        val uid = FirebaseAuth.getInstance().uid
        FirebaseFirestore.getInstance().collection("transaksi").whereEqualTo(rowTransaksi.K_IDUSER,uid.toString()).get()
            .addOnSuccessListener { results ->
                listTransaksi.clear()
                for (doc in results){
                    val transaksi = Transaksi()
                    transaksi.idTransaksi = doc.get(rowTransaksi.K_IDTRANSAKSI).toString()
                    transaksi.id_user = doc.get(rowTransaksi.K_IDUSER).toString()
                    transaksi.tgl = doc.get(rowTransaksi.K_TGL).toString()
                    transaksi.total = doc.get(rowTransaksi.K_TOTAL).toString()
                    transaksi.bukti_pembayaran = doc.get(rowTransaksi.K_BUKTI_PEMBAYARAN).toString()
                    transaksi.status =  doc.get(rowTransaksi.K_STATUS).toString()
                    listTransaksi.add(transaksi)
                }
                v.recyclerView_Transaksi.adapter = TransaksiAdapter(listTransaksi,thisParent,this)
            }
    }
    override fun onStart() {
        super.onStart()
        showListTransaksi()
    }

    fun gotoDetailtoTransaksi(list : Transaksi){
        val intent = Intent(thisParent, DetailTransaksiActivity::class.java)
        intent.putExtra("DetTransaksi",list)
        startActivity(intent)
    }


}