package bayu.risfandi.uts.screen

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import bayu.risfandi.uts.MainActivity
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.User
import bayu.risfandi.uts.util.Komponen
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    val user = User()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initView()
    }

    private fun initView() {
        button_register.setOnClickListener {
            register()
        }
    }

    private fun register(){
        val email = editTextEmailRegister.text.toString()
        val password = editTextPasswordRegister.text.toString()
        Komponen.ProgressLoading(true,"Loading....",this)
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if(it.isSuccessful){
                    insertDataUser(0)
                }
            }
            .addOnFailureListener {
                Komponen.ProgressLoading(false,"",this)
                Toast.makeText(this,"Failed to create user: ${it.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun insertDataUser(tipeuser : Int){
        val uid = FirebaseAuth.getInstance().uid ?: ""
        user.iduser = uid
        user.namaLengkap = editTextNamaRegister.text.toString()
        user.tipeuser = 0
        user.alamat = editTextAlamatRegister.text.toString()
        user.telepon = editTextTelepon.text.toString()
        val email = editTextEmailRegister.text.toString()
        FirebaseFirestore.getInstance().collection("users").document(email).set(user)
            .addOnSuccessListener {
                Komponen.ProgressLoading(false,"",this)
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            .addOnFailureListener {
                Toast.makeText(this, "Data failed to added ${it.message}",Toast.LENGTH_LONG).show()
            }
    }

}