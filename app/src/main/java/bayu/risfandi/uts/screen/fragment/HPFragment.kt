package bayu.risfandi.uts.screen.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bayu.risfandi.uts.MainActivity
import bayu.risfandi.uts.R
import bayu.risfandi.uts.adapter.ProdukAdapter
import bayu.risfandi.uts.model.Produk
import bayu.risfandi.uts.screen.produk.DetailProdukActivity
import bayu.risfandi.uts.screen.produk.TambahProdukActivity
import bayu.risfandi.uts.util.RowProduk
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_h_p.view.*


class HPFragment : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var db : CollectionReference
    var listProduct : MutableList<Produk> = ArrayList()
    val rowProduk = RowProduk()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_h_p, container, false)
        initView()
        return v
    }

    private fun initView() {
        v.fab_add_product.setOnClickListener {
            val intent = Intent(thisParent, TambahProdukActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showDataProduk(){
        db = FirebaseFirestore.getInstance().collection("products")
        db.addSnapshotListener { value, error ->
            if(error != null){
                Log.e("firestore : ",error.message.toString())
            }
            db.get().addOnSuccessListener { results ->
                listProduct.clear()
                for (doc in results){
                    val produk = Produk()
                    produk.merkProduk = doc.get(rowProduk.K_MERKPRODUK).toString()
                    produk.idProduk = doc.get(rowProduk.K_IDPRODUK).toString()
                    produk.stokProduk = doc.get(rowProduk.K_STOKPRODUK).toString().toInt()
                    produk.deskProduk = doc.get(rowProduk.K_DESKPRODUK).toString()
                    produk.hargaProduk = doc.get(rowProduk.K_HARGAPRODUK).toString()
                    produk.namaFile = doc.get(rowProduk.K_NAMAFILE).toString()
                    produk.fileProduk = doc.get(rowProduk.K_FILEPRODUK).toString()
                    listProduct.add(produk)
                }
                v.recyclerView_DataHP.adapter = ProdukAdapter(listProduct,thisParent,this)
            }
        }
    }

    fun gotoDetail(list : Produk){
        val intent = Intent(thisParent, DetailProdukActivity::class.java)
        intent.putExtra("DetProduk",list)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        showDataProduk()
    }




}