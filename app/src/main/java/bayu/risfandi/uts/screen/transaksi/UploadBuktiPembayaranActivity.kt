package bayu.risfandi.uts.screen.transaksi

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import bayu.risfandi.uts.R
import bayu.risfandi.uts.util.Komponen
import bayu.risfandi.uts.util.RowTransaksi
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_upload_bukti_pembayaran.*
import java.text.SimpleDateFormat
import java.util.*

class UploadBuktiPembayaranActivity : AppCompatActivity() {
    lateinit var uri : Uri
    var idTransaksi = ""
    val rowTransaksi = RowTransaksi()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_bukti_pembayaran)

        var paket : Bundle? = intent.extras
        idTransaksi = paket?.getString("idTransaksi").toString()

        initView()
    }

    private fun initView() {
        supportActionBar?.title = "Upload Bukti Pembayaran"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        button_pilih_file_upload_bukti.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_GET_CONTENT
            intent.setType("image/*")
            startActivityForResult(intent, 100)
        }

        button_upload_file_upload_bukti.setOnClickListener {
            uploadBuktiTransaksi()
        }

    }

    private fun uploadBuktiTransaksi(){
        Komponen.ProgressLoading(true,"Harap Tunggu...",this)
        val fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
        val fileRef = FirebaseStorage.getInstance().reference.child(fileName+".jpg")
        fileRef.putFile(uri)
            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                return@Continuation fileRef.downloadUrl
            })
            .addOnCompleteListener { task ->
                val hm = HashMap<String, Any>()
                hm.set(rowTransaksi.K_BUKTI_PEMBAYARAN,task.result.toString())
                hm.set(rowTransaksi.K_STATUS,"yes")
                updateStatusTransaksi(idTransaksi,hm)
            }
    }

    private fun updateStatusTransaksi(idTransaksi : String, hm : HashMap<String, Any>){
        FirebaseFirestore.getInstance().collection("transaksi").document(idTransaksi).update(hm)
            .addOnSuccessListener {
                Komponen.ProgressLoading(false,"",this)
                onBackPressed()
            }
            .addOnFailureListener {
                Toast.makeText(this, "Data failed to added ${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if((resultCode == Activity.RESULT_OK) && (requestCode == 100)){
            if(data != null){
                uri = data.data!!
                textView_bukti_pembayaran.setText(uri.toString())
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}