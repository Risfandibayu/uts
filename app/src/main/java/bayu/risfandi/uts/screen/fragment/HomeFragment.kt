package bayu.risfandi.uts.screen.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import bayu.risfandi.uts.MainActivity
import bayu.risfandi.uts.R
import bayu.risfandi.uts.adapter.ListProdukHomeAdapter
import bayu.risfandi.uts.model.Produk
import bayu.risfandi.uts.screen.produk.DetailProdukToBuyActivity
import bayu.risfandi.uts.util.RowProduk
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {
    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var db : CollectionReference
    var listProduct : MutableList<Produk> = ArrayList()
    val rowProduk = RowProduk()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_home, container, false)
        showDataProduk()
        v.recyclerView_DataHome.setLayoutManager(GridLayoutManager(thisParent,2))
        return v
    }

    private fun showDataProduk(){
        db = FirebaseFirestore.getInstance().collection("products")
        db.addSnapshotListener { value, error ->
            if(error != null){
                Log.e("firestore : ", error.message.toString())
            }
            db.get().addOnSuccessListener { results ->
                listProduct.clear()
                for (doc in results){
                    val produk = Produk()
                    produk.merkProduk = doc.get(rowProduk.K_MERKPRODUK).toString()
                    produk.idProduk = doc.get(rowProduk.K_IDPRODUK).toString()
                    produk.stokProduk = doc.get(rowProduk.K_STOKPRODUK).toString().toInt()
                    produk.deskProduk = doc.get(rowProduk.K_DESKPRODUK).toString()
                    produk.hargaProduk = doc.get(rowProduk.K_HARGAPRODUK).toString()
                    produk.namaFile = doc.get(rowProduk.K_NAMAFILE).toString()
                    produk.fileProduk = doc.get(rowProduk.K_FILEPRODUK).toString()
                    listProduct.add(produk)
                }
                v.recyclerView_DataHome.adapter = ListProdukHomeAdapter(listProduct, thisParent, this)
            }
        }
    }

    fun gotoDetailtoBuy(list : Produk){
        val intent = Intent(thisParent, DetailProdukToBuyActivity::class.java)
        intent.putExtra("DetProduktoBuy",list)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        showDataProduk()
    }


}