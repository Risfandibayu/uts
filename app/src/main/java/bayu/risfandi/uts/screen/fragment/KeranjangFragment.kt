package bayu.risfandi.uts.screen.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import bayu.risfandi.uts.MainActivity
import bayu.risfandi.uts.R
import bayu.risfandi.uts.adapter.KeranjangAdapter
import bayu.risfandi.uts.model.DetTransaksi
import bayu.risfandi.uts.model.Keranjang
import bayu.risfandi.uts.model.Transaksi
import bayu.risfandi.uts.util.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_keranjang.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class KeranjangFragment : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View
    //    val keranjang = Keranjang()
    lateinit var db : CollectionReference
    val transaksi = Transaksi()
    val rowProduk = RowProduk()
    val rowKeranjang = RowKeranjang()
    var listKeranjang : MutableList<Keranjang> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_keranjang, container, false)
        initView()
        return v
    }

    private fun initView() {
        v.button_checkout_transaksi.setOnClickListener {
            finishTransaksi()
        }
        v.button_uncheckout_transaksi.setOnClickListener {
            clearKeranjang()
        }
    }

    private fun showListKeranjang(){
        val uid = FirebaseAuth.getInstance().uid
        FirebaseFirestore.getInstance().collection("products").get()
            .addOnSuccessListener { results ->
                listKeranjang.clear()
                var totalBayar =  0
                for (product in results){
                    FirebaseFirestore.getInstance().collection("keranjang")
                        .whereEqualTo(rowKeranjang.K_IDUSER,uid.toString())
                        .whereEqualTo(rowKeranjang.K_IDPRODUK,product.get(rowProduk.K_IDPRODUK)).get()
                        .addOnSuccessListener { results ->
                            for (doc in results){
                                val keranjang = Keranjang()
                                val total = PisahAngka.getAngkaRupiah(doc.get(rowKeranjang.K_HARGAPRODUK) as String).toInt() * doc.get(rowKeranjang.K_QTY).toString().toInt()
                                totalBayar = totalBayar + total
                                keranjang.idKeranjang = doc.get(rowKeranjang.K_IDKERANJANG).toString()
                                keranjang.idProduk = doc.get(rowKeranjang.K_IDPRODUK).toString()
                                keranjang.hargaProduk = total.toString()
                                keranjang.namaProduk = product.get(rowProduk.K_MERKPRODUK).toString()
                                keranjang.qty = doc.get(rowKeranjang.K_QTY).toString().toInt()
                                listKeranjang.add(keranjang)
                            }
                            v.recylerView_Keranjang.adapter = KeranjangAdapter(listKeranjang,thisParent,this)
                            v.textView_total_transaksi.text = totalBayar.toString()
                        }
                }
            }
    }

    fun addQtyItemKeranjang(idkeranjang : String, qty : Int){
        var cqty = qty + 1
        val hm = HashMap<String, Any>()
        hm.set(rowKeranjang.K_IDKERANJANG,idkeranjang)
        hm.set(rowKeranjang.K_QTY,cqty)
        FirebaseFirestore.getInstance().collection("keranjang").document(idkeranjang).update(hm)
            .addOnSuccessListener {
                showListKeranjang()
            }
            .addOnFailureListener {
                Toast.makeText(thisParent, "Data failed to added ${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    fun kurangQtyItemKeranjang(idkeranjang : String, qty : Int){
        var cqty = qty - 1
        val hm = HashMap<String, Any>()
        hm.set(rowKeranjang.K_IDKERANJANG,idkeranjang)
        hm.set(rowKeranjang.K_QTY,cqty)
        FirebaseFirestore.getInstance().collection("keranjang").document(idkeranjang).update(hm)
            .addOnSuccessListener {
                showListKeranjang()
            }
            .addOnFailureListener {
                Toast.makeText(thisParent, "Data failed to added ${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    private fun finishTransaksi() {
        Komponen.ProgressLoading(false,"Memproses Transaksi....",thisParent)
        val uid = FirebaseAuth.getInstance().uid
        val idtransaksi = SimpleDateFormat("yyyyMMdd").format(Date())+uid
        transaksi.idTransaksi = idtransaksi
        transaksi.id_user = uid.toString()
        transaksi.status = "no"
        transaksi.bukti_pembayaran = "null"
        transaksi.total = v.textView_total_transaksi.text.toString()
        transaksi.tgl = SimpleDateFormat("dd/MM/yyyy").format(Date())
        FirebaseFirestore.getInstance().collection("transaksi").document(idtransaksi).set(transaksi).addOnSuccessListener {
            FirebaseFirestore.getInstance().collection("keranjang").whereEqualTo(rowKeranjang.K_IDUSER,uid).get()
                .addOnSuccessListener { results ->
                    for (doc in results){
                        val idDet = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())+ RandomAngka.getRandomString(5)
                        FirebaseFirestore.getInstance().collection("detailtransaksi").document(idDet).set(
                            DetTransaksi(idDet,idtransaksi,doc.get(rowKeranjang.K_IDPRODUK).toString(),uid.toString(),doc.get(rowKeranjang.K_QTY).toString())
                        )
                        FirebaseFirestore.getInstance().collection("keranjang").document(doc.id).delete()
                            .addOnSuccessListener {
                                Komponen.ProgressLoading(false,"",thisParent)
                                showListKeranjang()
                            }
                            .addOnFailureListener {
                                Toast.makeText(thisParent, "Data failed deleted", Toast.LENGTH_LONG).show()
                            }
                    }
                }
        }.addOnFailureListener {
            Toast.makeText(thisParent, it.message.toString(), Toast.LENGTH_LONG).show()
        }
    }

    private fun clearKeranjang(){
        val uid = FirebaseAuth.getInstance().uid
        FirebaseFirestore.getInstance().collection("keranjang").whereEqualTo(rowKeranjang.K_IDUSER,uid).get()
            .addOnSuccessListener { results ->
                for (doc in results){
                    FirebaseFirestore.getInstance().collection("keranjang").document(doc.id).delete()
                        .addOnSuccessListener {
                            Komponen.ProgressLoading(false,"",thisParent)
                            showListKeranjang()
                        }
                        .addOnFailureListener {
                            Toast.makeText(thisParent, "Data failed deleted", Toast.LENGTH_LONG).show()
                        }
                }
            }
    }

    override fun onStart() {
        super.onStart()
        showListKeranjang()
    }



}