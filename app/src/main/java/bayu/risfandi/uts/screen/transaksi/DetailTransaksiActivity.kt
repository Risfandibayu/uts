package bayu.risfandi.uts.screen.transaksi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import bayu.risfandi.uts.R
import bayu.risfandi.uts.adapter.DetilTransaksiAdapter
import bayu.risfandi.uts.model.DetTransaksi
import bayu.risfandi.uts.model.Transaksi
import bayu.risfandi.uts.util.RowDetilTransaksi
import bayu.risfandi.uts.util.RowProduk
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_detail_transaksi.*

class DetailTransaksiActivity : AppCompatActivity() {

    var listTransaksi : MutableList<DetTransaksi> = ArrayList()
    val rowDetTransaksi = RowDetilTransaksi()
    val rowProduk = RowProduk()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaksi)

        initView()
    }

    private fun initView() {
        supportActionBar?.title = "Detail Transaksi"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val transaksi = intent.getParcelableExtra<Transaksi>("DetTransaksi")
        textView_tglTransaksi.setText(transaksi?.tgl)
        textView_Pembeli.setText(transaksi?.id_user)

        var status = ""
        if(transaksi?.status.equals("no")){
            status = "Belum Lunas"
            textView_goto_upload_bukti.visibility = View.VISIBLE
            textView_goto_upload_bukti.setOnClickListener {
                val intent = Intent(this, UploadBuktiPembayaranActivity::class.java)
                intent.putExtra("idTransaksi",transaksi?.idTransaksi)
                startActivityForResult(intent,21)
            }
            textView_status_transaksi.setText(status)
        }else if(transaksi?.status.equals("yes")){
            status = "Lunas"
            textView_goto_upload_bukti.visibility = View.GONE
            textView_status_transaksi.setText(status)
        }

        if(transaksi?.bukti_pembayaran.equals("null")){
            textView_buktiPembayaran_detil.setText("Belum terbayar")
        }else{
            textView_buktiPembayaran_detil.setText("terbayar")
        }

        textView_total_transaksi.setText(transaksi?.total)

    }

    private fun showDetailTransaksi(){
        val transaksi = intent.getParcelableExtra<Transaksi>("DetTransaksi")
        FirebaseFirestore.getInstance().collection("detailtransaksi").whereEqualTo(rowDetTransaksi.K_ID_TRANSAKSI,transaksi?.idTransaksi).get()
            .addOnSuccessListener { results ->
                listTransaksi.clear()
                for (doc in results){

                    FirebaseFirestore.getInstance().collection("products")
                        .whereEqualTo(rowProduk.K_IDPRODUK,doc.get(rowDetTransaksi.K_ID_PRODUCT)).get()
                        .addOnSuccessListener { results ->
                            for (product in results) {
                                val transaksi = DetTransaksi()
                                transaksi.id_product = product.get(rowProduk.K_MERKPRODUK).toString()
                                transaksi.qty = doc.get(rowDetTransaksi.K_QTY).toString()
                                transaksi.id_user = product.get(rowProduk.K_HARGAPRODUK).toString()
                                listTransaksi.add(transaksi)
                            }
                            recyclerVeiw_Data_DetilTransaksi.adapter =
                                DetilTransaksiAdapter(listTransaksi,this)
                        }

                }
            }
    }

    override fun onStart() {
        super.onStart()
        showDetailTransaksi()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}