package bayu.risfandi.uts.util

class RandomAngka {
    companion object {
        fun getRandomString(length: Int) : String {
            val charset = ('a'..'z') + ('A'..'Z') + ('0'..'9')

            return List(length) { charset.random() }
                .joinToString("")
        }
    }
}