package bayu.risfandi.uts.util

class RowProduk {
    val K_IDPRODUK = "idProduk"
    val K_MERKPRODUK = "merkProduk"
    val K_HARGAPRODUK = "hargaProduk"
    val K_DESKPRODUK = "deskProduk"
    val K_STOKPRODUK = "stokProduk"
    val K_NAMAFILE = "namaFile"
    val K_FILEPRODUK = "fileProduk"
}