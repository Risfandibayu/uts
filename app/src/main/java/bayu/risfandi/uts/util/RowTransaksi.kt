package bayu.risfandi.uts.util

class RowTransaksi {
    val K_IDTRANSAKSI = "idTransaksi"
    val K_IDUSER = "id_user"
    val K_STATUS = "status"
    val K_TGL = "tgl"
    val K_TOTAL = "total"
    val K_BUKTI_PEMBAYARAN = "bukti_pembayaran"
}