package bayu.risfandi.uts.util

class PisahAngka {
    companion object {
        fun getAngkaRupiah(uang : String):String{
            val string1 = uang.replace("\\s".toRegex(),"")
            val string2 = string1.replace("Rp","")
            val string3 = string2.replace(".","")
            return string3
        }
    }
}