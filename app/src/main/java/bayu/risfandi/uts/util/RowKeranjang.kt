package bayu.risfandi.uts.util

class RowKeranjang {
    val K_IDKERANJANG = "idKeranjang"
    val K_IDPRODUK = "idProduk"
    val K_IDUSER = "idUser"
    val K_HARGAPRODUK = "hargaProduk"
    val K_NAMAPRODUK = "namaProduk"
    val K_QTY = "qty"
}