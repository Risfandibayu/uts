package bayu.risfandi.uts.util

import android.app.ProgressDialog
import android.content.Context

class Komponen {
    companion object {
        fun ProgressLoading(show : Boolean, message: String, context: Context){
            val progressDialog = ProgressDialog(context)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage(message)
            if(show){
                progressDialog.show()
            }else{
                progressDialog.hide()
            }
        }
    }
}