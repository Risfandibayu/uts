package bayu.risfandi.uts.util

class RowDetilTransaksi {
    val K_IDDET_TRANSAKSI = "idDetTransaksi"
    val K_ID_PRODUCT = "id_product"
    val K_ID_TRANSAKSI = "id_transaksi"
    val K_ID_USER = "id_user"
    val K_QTY = "qty"
}