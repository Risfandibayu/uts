package bayu.risfandi.uts.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Keranjang
import bayu.risfandi.uts.screen.fragment.KeranjangFragment
import kotlinx.android.synthetic.main.row_list_keranjang.view.*

class KeranjangAdapter (items:List<Keranjang>, context: Context, val itemKlik: KeranjangFragment): RecyclerView.Adapter<KeranjangAdapter.ViewHolder>(){

    private var list = items
    private var context = context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): KeranjangAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_list_keranjang,parent,false))
    }

    override fun onBindViewHolder(holder: KeranjangAdapter.ViewHolder, position: Int) {
        holder.merkProduk.text = list[position].namaProduk
        holder.hargaProduk.text = list[position].hargaProduk
        holder.qtyProduk.text = list[position].qty.toString()
        holder.btnTambahKeranjang.setOnClickListener {
            itemKlik.addQtyItemKeranjang(list[position].idKeranjang,list[position].qty)
        }
        holder.btnKurangKeranjang.setOnClickListener {
            if(list[position].qty >= 1){
                itemKlik.kurangQtyItemKeranjang(list[position].idKeranjang,list[position].qty)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val merkProduk = v.textView_Merk_Keranjangan!!
        val hargaProduk = v.textView_Total_Keranjang!!
        val qtyProduk = v.textView_QTY_Keranjang!!
        val btnTambahKeranjang = v.button_add_keranjang!!
        val btnKurangKeranjang = v.button_kurang_keranjang!!
    }
}