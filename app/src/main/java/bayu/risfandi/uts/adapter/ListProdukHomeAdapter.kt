package bayu.risfandi.uts.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Produk
import bayu.risfandi.uts.screen.fragment.HomeFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_list_produk_home.view.*

class ListProdukHomeAdapter (items:List<Produk>, context: Context, val itemKlik: HomeFragment): RecyclerView.Adapter<ListProdukHomeAdapter.ViewHolder>(){

    private var list = items
    private var context = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListProdukHomeAdapter.ViewHolder {
        return ListProdukHomeAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_list_produk_home, parent, false))
    }

    override fun onBindViewHolder(holder: ListProdukHomeAdapter.ViewHolder, position: Int) {
        holder.merkProduk.text = list[position].merkProduk
        holder.hargaProduk.text = list[position].hargaProduk
        Picasso.get().load(list[position].fileProduk).into(holder.imageProduk)
        holder.root_produk_home.setOnClickListener {
            itemKlik.gotoDetailtoBuy(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val merkProduk = v.textView_Merk_Produk!!
        val hargaProduk = v.textView_Harga_Produk!!
        val imageProduk = v.imageView_Produk_Home!!
        val root_produk_home = v.root_produk_home!!
    }
}