package bayu.risfandi.uts.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Transaksi
import bayu.risfandi.uts.screen.fragment.TransaksiFragment
import kotlinx.android.synthetic.main.row_list_transaksi.view.*

class TransaksiAdapter (items:List<Transaksi>, context: Context, val itemKlik: TransaksiFragment): RecyclerView.Adapter<TransaksiAdapter.ViewHolder>(){

    private var list = items
    private var context = context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransaksiAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_list_transaksi,parent,false))
    }

    override fun onBindViewHolder(holder: TransaksiAdapter.ViewHolder, position: Int) {
        holder.tglTransaksi.text = list[position].tgl
        holder.totalTransaksi.text = list[position].total
        holder.rootTransaksi.setOnClickListener {
            itemKlik.gotoDetailtoTransaksi(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val tglTransaksi = v.textView_tgl_Tr!!
        val totalTransaksi = v.textView_total_transaksi!!
        val rootTransaksi = v.root_my_transaksi!!
    }
}