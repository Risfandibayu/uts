package bayu.risfandi.uts.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.Produk
import bayu.risfandi.uts.screen.fragment.HPFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_produk.view.*

class ProdukAdapter  (items:List<Produk>, context: Context, val itemKlik: HPFragment): RecyclerView.Adapter<ProdukAdapter.ViewHolder>(){

    private var list = items
    private var context = context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProdukAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_produk,parent,false))
    }

    override fun onBindViewHolder(holder: ProdukAdapter.ViewHolder, position: Int) {
        holder.merkProduk.text = list[position].merkProduk
        holder.hargaProduk.text = list[position].hargaProduk
        Picasso.get().load(list[position].fileProduk).into(holder.imageProduk)
        holder.root_item_produk.setOnClickListener {
            itemKlik.gotoDetail(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val merkProduk = v.textView_MerkProduk!!
        val hargaProduk = v.textView_HargaProduk!!
        val imageProduk = v.imageView_Produk!!
        val root_item_produk = v.root_item_produk!!
    }
}