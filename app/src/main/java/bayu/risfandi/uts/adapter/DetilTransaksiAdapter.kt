package bayu.risfandi.uts.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bayu.risfandi.uts.R
import bayu.risfandi.uts.model.DetTransaksi
import kotlinx.android.synthetic.main.row_list_detail_transaksi.view.*

class DetilTransaksiAdapter (items:List<DetTransaksi>, context: Context): RecyclerView.Adapter<DetilTransaksiAdapter.ViewHolder>(){

    private var list = items
    private var context = context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetilTransaksiAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_list_detail_transaksi,parent,false))
    }

    override fun onBindViewHolder(holder: DetilTransaksiAdapter.ViewHolder, position: Int) {
        holder.merkProduk.text = list[position].id_product
        holder.stokDetail.text = list[position].qty
        holder.hargaDetail.text = list[position].id_user // mengakali untuk tampilkan harga barang
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val merkProduk = v.textView_Produk_detil_transaksi!!
        val stokDetail = v.textView_stok_detail!!
        val hargaDetail = v.textView_harga_brg!!
    }
}