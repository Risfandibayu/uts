package bayu.risfandi.uts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.FragmentTransaction
import bayu.risfandi.uts.screen.LoginActivity
import bayu.risfandi.uts.screen.RegisterActivity
import bayu.risfandi.uts.screen.fragment.HPFragment
import bayu.risfandi.uts.screen.fragment.HomeFragment
import bayu.risfandi.uts.screen.fragment.KeranjangFragment
import bayu.risfandi.uts.screen.fragment.TransaksiFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),  BottomNavigationView.OnNavigationItemSelectedListener{


    lateinit var ft : FragmentTransaction
    lateinit var hpFragment: HPFragment
    lateinit var transaksiFragment: KeranjangFragment
    lateinit var homeFragment: HomeFragment
    lateinit var MyTransaksi: TransaksiFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        hpFragment = HPFragment()
        transaksiFragment = KeranjangFragment()
        homeFragment = HomeFragment()
        MyTransaksi = TransaksiFragment()
        initView()
        cekUserIsLogin()
    }

    private fun initView() {
        supportActionBar!!.title = "Beranda"
        bottomNavigationView2.setOnNavigationItemSelectedListener(this)
    }

    private fun cekUserIsLogin(){
        val uid = FirebaseAuth.getInstance().uid
        if(uid == null){
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_logout,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId) {
            R.id.menu_logout -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_barang -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, hpFragment).commit()
                supportActionBar!!.title = "Barang"
            }
            R.id.menu_transaksi -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, transaksiFragment).commit()
                supportActionBar!!.title = "Keranjang"
            }
            R.id.menu_home -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, homeFragment).commit()
                supportActionBar!!.title = "Beranda"
            }
            R.id.menu_main_transaksi -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, MyTransaksi).commit()
                supportActionBar!!.title = "Transaksi"
            }
        }
        return true
    }

}